(function () {
    angular
        .module("BookApp")
        .service("bookSvc", bookSvc);
    
    function bookSvc($http, $q) {
        var vm = this;
        
        vm.list = function (limit, offset) {
            var defer = $q.defer();
            var params = {
                limit: limit || 20,
                offset: offset || 0
            };
            $http.get("/api/books",{
                params: params
            }).then(function (result) {
                    defer.resolve(result.data);
            }).catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };
        
        vm.edit = function (bookId) {
            var defer = $q.defer();
            $http.get("/api/book/" + bookId)
                .then(function (result) {
                    defer.resolve(result.data);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };
        
        vm.save = function (book) {
            var defer = $q.defer();
            $http.post("/api/book/save")
                .then(function (result) {
                    defer.resolve(result);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };
        
        vm.search = function (title, name) {
            var defer = $q.defer();
            param = {title: title, name: name};
            $http.get("/api/book/search", {
                    params: param
                })
                .then(function (results) {
                    defer.resolve(results.data);
                })
                .catch(function (err) {
                    console.log(err);
                });
            return defer.promise;
        };
        
    }
    bookSvc.$inject = ["$http", "$q"];
    
})();